//SOAL 1/////////////////////////////////////////////////

function range(startNum, finishNum) {
	var temp = [];
	var x = 0;
 	if(startNum<finishNum){
 		for(i=startNum;i<=finishNum;i++){
 			temp.push(i);
 		}
 	} else if(startNum>finishNum){
 		x = startNum;
 	 	for(i=finishNum;i<=startNum;i++){
 			temp.push(x);
 			x = x - 1; 
 		}
 	} else if(startNum == finishNum){
 		temp.push(startNum);
 	} else {
 		temp.push(-1);
 	}
 	return temp;
 } 

console.log(range(10,5));

//SOAL 2//////////////////////////////////////////////////

function rangeWithStep(startNum, finishNum,step) {
	var temp = [];
	if(startNum<finishNum){
	 	while (startNum<=finishNum){
	 		temp.push(startNum);
	 		startNum = startNum + step
	 	}
	} else if(startNum>finishNum){
		while (startNum>=finishNum){
	 		temp.push(startNum);
	 		startNum = startNum - step
	 	}
	} else if(startNum==finishNum){
		temp.push(startNum);
	} else {
		temp.push(-1);
	}
 	return temp;	
 }

console.log(rangeWithStep(20,1,3));

//SOAL 3/////////////////////////////////////////////////

const sum = (startNum = null, finishNum = null, step = 1) => {
	var tot = 0;
	if ((startNum == null)&&(finishNum == null)&&(step == 1)){
		tot = 0
	} else if(startNum<finishNum){
	 	while (startNum<=finishNum){
	 		tot = tot + startNum;
	 		startNum = startNum + step	 		 
	 	}
	} else if(startNum>finishNum){
		while (startNum>=finishNum){
	 		tot = tot + startNum;
	 		startNum = startNum - step
	 	}
	} else if(startNum==finishNum){
		tot = startNum;
	} else {		
		tot = 1;
	}
 	return tot;	
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// SOAL 4 /////////////////////////////////////////////////////

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 

var param = ["Nomor ID : ", "Nama Lengkap : ", "TTL : "," ", "Hobi : "]

console.log(dataHandling(input,param));

function dataHandling(input){
	var temp = []
	var ulang = input.length
	var hasil = ""
	for(i=0;i<ulang;i++){
		var count = input[i].length
		for(j=0;j<count;j++){
			
			if (j==2){
				hasil += param[j]
				hasil += input[i][j]
			} else if (j!=3){
				hasil += param[j]
				hasil += input[i][j]
				hasil += '\n'
				// hasil += '\n'
			} else if(j==3){
				hasil += param[j]
				hasil += input[i][j]
				hasil += '\n'
			}	
		}
		hasil += '\n'		
	}
	return hasil
}

//SOAL 5///////////////////////////////////////////////////////////////////////////////


function balikKata(str) {
var newString = "";
	for (var i = str.length - 1; i >= 0; i--) { 
		newString += str[i];
	}
	return newString;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


// SOAL 6///////////////////////////////////////////////////////////////////////////

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2 (input){

	input.splice(1,1,"Roman Alamsyah Elsharawy")
	input.splice(2,1,"Provinsi Bandar Lampung")
	input.splice(4,0,"Pria")
	input.splice(5,1,"SMA Internasional Metro")

	console.log(input)

	var x = input[3]
	var split = x.split("/")
	var bulan = split[1]

	switch(bulan) {
	 case "01": bulan = "Januari"; break;
	 case "02": bulan = "Februari"; break;
	 case "03": bulan = "Maret"; break;
	 case "04": bulan = "April"; break;
	 case "05": bulan = "Mei"; break;
	 case "06": bulan = "Juni"; break;
	 case "07": bulan = "Juli"; break;
	 case "08": bulan = "Agustus"; break;
	 case "09": bulan = "September"; break;
	 case "10": bulan = "Oktober"; break;
	 case "11": bulan = "November"; break;
	 case "12": bulan = "Desember"; break;
	}

	console.log(bulan)

	var sort = x.split("/").sort(function (value1, value2) { return value2 - value1 } ) ;
	console.log(sort)

	var join = x.split("/").join("-")
	console.log(join)

	var batas = input[1].slice(0,14)
	console.log(batas)


}

dataHandling2(input);

















