//SOAL No 1/////////////////////////////////////////////////

var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

var arr = [word,second,third,fourth,fifth,sixth,seventh];
var str = arr.join(' ');
console.log(str);

//SOAL No 2////////////////////////////////////////////////

var sentence = "I am going to be React Native Developer"; 
var ar = sentence.split(' ');
console.log( ar );

var firstWord = ar[0] ; 
var secondWord = ar[1] ; 
var thirdWord = ar[2] ;
var fourthWord = ar[3] ;
var fifthWord = ar[4] ;
var sixthWord = ar[5] ;
var seventhWord = ar[6] ;
var eighthWord = ar[7] ;

console.log('First Word: ' + firstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)

//SOAL No 3///////////////////////////////////////////////

var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2= sentence2.substring(4, 14); 
var thirdWord2= sentence2.substring(15, 17);  
var fourthWord2= sentence2.substring(18, 20); 
var fifthWord2= sentence2.substring(21, 25); 

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

//SOAL No 4/////////////////////////////////////////////

var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2= sentence2.substring(4, 14); 
var thirdWord2= sentence2.substring(15, 17);  
var fourthWord2= sentence2.substring(18, 20); 
var fifthWord2= sentence2.substring(21, 25); 

console.log('First Word: ' + exampleFirstWord2+', with length: '+exampleFirstWord2.length); 
console.log('Second Word: ' + secondWord2+', with length: '+secondWord2.length);
console.log('Third Word: ' + thirdWord2+', with length: '+thirdWord2.length);
console.log('Fourth Word: ' + fourthWord2+', with length: '+fourthWord2.length);
console.log('Fifth Word: ' + fifthWord2+', with length: '+fifthWord2.length);










