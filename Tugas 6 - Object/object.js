
//SOAL 2//////////////////////////////////////////////////////////////////////////////////

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]

function arrayToObject(people) {
	var jum = people.length
	var now = new Date()
	var thisYear = now.getFullYear()

	for(i=0;i<jum;i++){
		var count = i+1
		var ulang = people[i].length
		if(people[i][3] > thisYear){
			var umur = "Invalid birth year"
		} else if( typeof  people[i][3] === 'number'){
			var umur = thisYear - people[i][3]
		} else {
			var umur = "Invalid birth year"
		}		
		var obj = {
			firstName : people[i][0],
		    lastName : people[i][1],
		    gender : people[i][2],
		    age : umur
		}
		console.log(count+'. '+obj.firstName+" "+obj.lastName+" : " , obj)
	}

}

arrayToObject(people);
arrayToObject(people2);

//SOAL 2/////////////////////////////////////////////////////////////////////////////////

function shoppingTime(memberId, money) {
  // you can only write your code here!
  var obj = new Object();
  var sisa = money
  var list = []
  var i = 0
  var temp = ""
  while(sisa>=50000){
  	if ((sisa>=1500000)&&(temp!="Sepatu Stacattu")){
  		sisa = sisa - 1500000
  		list.push("Sepatu Stacattu")
  		temp = "Sepatu Stacattu"
  	} else if((sisa>=500000)&&(temp!="Baju Zoro")){
  		sisa = sisa - 500000
  		list.push("Baju Zoro")
  		temp = "Baju Zoro"
  	} else if((sisa>=250000)&&(temp!="Baju H&N")){
  		sisa = sisa - 250000
  		list.push("Baju H&N")
  		temp = "Baju H&N"
  	} else if((sisa>=175000)&&(temp!="Sweater Uniklooh")){
  		sisa = sisa - 175000
  		list.push("Sweater Uniklooh")
  		temp = "Sweater Uniklooh"
  	} else if((sisa>=50000)&&(temp!="Casing Handphone")){
  		sisa = sisa - 50000
  		list.push("Casing Handphone")
  		temp = "Casing Handphone"
  	} else {
  		break
  	}
  	i++
  }

  obj.memberId = memberId
  obj.money = money
  obj.listPurchased = list
  obj.changeMoney = sisa

  return obj
}

console.log(shoppingTime('82Ku8Ma742', 2475000))
console.log(shoppingTime('82Ku8Ma742', 170000));

//SOAL 3/////////////////////////////////////////////////////////////////////

function naikAngkot(arrPenumpang = 0) {
  var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var jum = arrPenumpang.length;
  var count_asl = 0;
  var count_tjn = 0;
  var obj = {}
  var list = []

  if(arrPenumpang != 0){
	  for(i=0;i<jum;i++){
	  	
	  	var asl = arrPenumpang[i][1]
	  	var tjn = arrPenumpang[i][2]
	  	for(j=0;j<rute.length;j++){
	  		if(asl == rute[j]){
	  			count_asl = j
	  		}
	  		if(tjn == rute[j]){
	  			count_tjn = j
	  		}
	  	}
	  	var selisih = count_tjn - count_asl
	  	list.push({
	  		penumpang : arrPenumpang[i][0],
	  		naikDari : asl,
	  		tujuan : tjn,
	  		bayar : selisih*2000
	  	})
	  	obj = list
	  }
	  
	  return obj
	} else {
		return list
	}
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));
























