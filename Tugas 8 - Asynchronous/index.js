// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var count = books.length
var i = 0;
var waktu = 10000;
sisa = 0;

readBooks(waktu,books[i], sisa=>{
	readBooks(sisa,books[i+1], sisa=>{
		readBooks(sisa,books[i+2], sisa=>{
			return sisa
		});
	});
});