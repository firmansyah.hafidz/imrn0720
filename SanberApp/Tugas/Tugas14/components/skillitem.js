import React from 'react';
import {
	View, 
	Text, 
	StyleSheet,
	Image,
	TouchableOpacity,
	FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons'; 


export default class SkillItem extends React.Component {

	render(){
		let video = this.props.video;
		return(
			<View style={styles.container}>

				<View style={styles.sampingContent}>
					<MaterialCommunityIcons name={video.iconName} size={75} color="#003366"/>
					<View>
						<Text style={{fontSize:24, color:'#003366'}}>{video.skillName}</Text>
						<Text style={{fontSize:16, color:'#3EC6FF'}}>{video.categoryName}</Text>
						<Text style={{fontSize:30, color:'white'}}>{video.percentageProgress}</Text>
					</View>
					<AntDesign name="right" size={50} color="#003366" />
				</View>
			</View>
		)
	}

}
const styles = StyleSheet.create({
	container:{
		padding: 15
	},
	sampingContent:{
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems:'center'
	},
	descContainer:{
		flexDirection: 'row',
		paddingTop: 15
	},
	videoTitle:{
		fontSize: 16,
		color: '#3C3C3C'
	},
	videoDetails:{
		paddingHorizontal: 15,
		flex: 1
	},
	videoStats:{
		fontSize: 14,
		paddingTop: 3
	},

});