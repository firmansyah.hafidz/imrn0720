import React from 'react';
import {
	View, 
	Text, 
	StyleSheet,
	Image,
	TouchableOpacity,
	FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './components/skillitem';
import data from './skillData.json';

export default class SkillScreen extends React.Component {
	render(){
		return (
			<View style={styles.container}>
				<View style={styles.logoStyle}>
					<Image source={require('./images/logo.png') }/>
				</View>
				<View style={styles.profile}>
					<Icon name="account-circle" size={50} color="#3EC6FF" />
					<View>
						<Text>Hai,</Text>
						<Text style={{fontSize:18, color:'#003366'}}>Hafidz Firmansyah</Text>
					</View>
				</View>
				<Text style={{fontSize:40, color:'#003366'}}>SKILL</Text>
				<View style={styles.kategori}>
					<Text style={styles.isiKategori}>Library/Framework </Text>
					<Text style={styles.isiKategori}>Bahasa Pemrograman </Text>
					<Text style={styles.isiKategori}>Teknologi </Text>
				</View>
				<View style={styles.body}>
					<FlatList
						data={data.items}
						renderItem={(video)=>
						<View style={styles.kotak}>
							<VideoItem video={video.item} />
						</View>
						}
						keyExtractor={(item)=>item.id}
						itemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
					/>
				</View>
				
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container:{
		flex: 1,
	},
	kategori:{
		marginRight:200,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		borderTopWidth: 3,
		borderColor: '#3EC6FF',
		width:350

	},
	isiKategori:{
		fontSize:15, 
		color:'#003366',
		backgroundColor:'#B4E9FF',
		marginBottom:5,
		marginLeft:1,
		marginRight:1,
		marginTop:5,

	},
	logoStyle:{
		marginTop: 10,
		marginLeft:45,
		alignItems: 'center',
		justifyContent: 'center'
	},profile:{
		marginRight:200,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between'
	},
	navBar:{
		height: 55,
		backgroundColor: 'white',
		elevation: 3,
		paddingHorizontal: 15,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between'
	},
	rightNav:{
		flexDirection: 'row'
	},
	navItem:{
		marginLeft: 25
	},
	body:{
		flex: 1,
	},
	kotak:{
		backgroundColor: '#B4E9FF',
		marginLeft:5,
		marginRight:5,
		borderRadius:10,
	    borderWidth: 1,
	    borderColor: '#fff'
	},
	tabBar:{
		backgroundColor: 'white',
		height: 60,
		borderTopWidth: 0.5,
		borderColor: '#E5E5E5',
		flexDirection: 'row',
		justifyContent: 'space-around'
	},
	tabItem:{
		alignItems: 'center',
		justifyContent: 'center'
	},
	tabTitle:{
		fontSize: 11,
		color: '#3c3c3c',
		paddingTop: 4
	}
	
})