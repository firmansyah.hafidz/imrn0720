import React from 'react';
import {
	View, 
	Text, 
	StyleSheet,
	Item,
	Form,
	Label,
	Input,
	Image,
	TouchableOpacity,
	Button,
	TextInput
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class LoginScreen extends React.Component<{}>  {
	render(){
		return(
			<View style={styles.container}>
				<View style={styles.logoStyle}>
					<Image source={require('./images/logo.png')}/>
					<Text style={styles.textLogin}>Login</Text>
				</View>
				<Text style={styles.textForm}>Username/Email</Text>
				<TextInput style={styles.inputStyle}/>
				<Text style={styles.textForm}>Password</Text>
				<TextInput style={styles.inputStyle}/>
				<View style={styles.buttonStyle}>
					<TouchableOpacity
					          style={styles.loginScreenButton1}					          
					          underlayColor='#fff'>
					          <Text style={styles.loginText}>Login</Text>
					 </TouchableOpacity>
					<Text style={styles.atau}>atau</Text>
					<TouchableOpacity
					          style={styles.loginScreenButton2}					          
					          underlayColor='#fff'>
					          <Text style={styles.loginText}>Daftar ?</Text>
					</TouchableOpacity>
				</View>

			</View>
		)
	}
}

const styles = StyleSheet.create({
	container:{
		padding: 15,
		flex: 1
	},
	logoStyle:{
		height: 90,
		width: 300,
		marginTop: 80,
		marginBottom: 80,
		alignItems: 'center',
		justifyContent: 'center'
	},
	textLogin:{
		fontSize: 25,
		marginTop: 30,
		color: '#003366'
	},
	textForm:{
		fontSize:15,
		marginLeft: 10,
		color: '#003366'
	},
	inputStyle:{
		height: 40,
		margin: 10,
		alignItems: 'center', 
		justifyContent: 'center',
		borderColor: '#003366', 
		borderWidth: 1
	},
	buttonStyle:{
		fontSize: 20,
		padding: 20
	},
	atau:{
		fontSize:15,
		color:'#3EC6FF',
		margin:10,
		textAlign:'center',
	},
	 loginScreenButton1:{
	    marginRight:40,
	    marginLeft:40,
	    marginTop:10,
	    paddingTop:10,
	    paddingBottom:10,
	    backgroundColor:'#3EC6FF',
	    borderRadius:10,
	    borderWidth: 1,
	    borderColor: '#fff'
	  },
	  loginScreenButton2:{
	    marginRight:40,
	    marginLeft:40,
	    marginTop:10,
	    paddingTop:10,
	    paddingBottom:10,
	    backgroundColor:'#003366',
	    borderRadius:10,
	    borderWidth: 1,
	    borderColor: '#fff'
	  },
	  loginText:{
	      color:'#fff',
	      textAlign:'center',
	      paddingLeft : 10,
	      paddingRight : 10
	  },
	
});