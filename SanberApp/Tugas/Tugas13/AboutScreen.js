import React from 'react';
import {
	View, 
	Text, 
	StyleSheet,
	Item,
	Form,
	Label,
	Input,
	Image,
	TouchableOpacity,
	Button,
	TextInput
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { MaterialCommunityIcons } from '@expo/vector-icons'; 

export default class AboutScreen extends React.Component<{}>  {
	render(){
		return(
			<View style={styles.container}>
				<Text style={styles.textForm}>Tentang Saya</Text>
				<MaterialCommunityIcons style={styles.profileIcon} name="account-circle" size={120} color="#EFEFEF" />
				<Text style={styles.textName}>Hafidz Firmansyah</Text>
				<Text style={styles.textDesc}>React Native Developer</Text>
				<View style={styles.portofolio}>
					<Text style={styles.textPorto}>Portofolio</Text>
					<View style={styles.isiPorto1}>
						<MaterialCommunityIcons name="gitlab" size={35} color="#3EC6FF" />
						<MaterialCommunityIcons name="github-circle" size={35} color="#3EC6FF" />
					</View>
					<View style={styles.isiPorto2}>						
						<Text style={styles.textProfile}>firmansyah.hafidz</Text>
						<Text style={styles.textProfile}>-</Text>
					</View>
					
				</View>
				<View style={styles.contact}>
					<Text style={styles.textPorto}>Hubungi Saya</Text>
					<View style={styles.bawah}>
						<MaterialCommunityIcons name="facebook" size={35} color="#3EC6FF" />
						<Text style={styles.textProfile}>Hafidz Firmansyah</Text>
					</View>
					<View style={styles.bawah}>
						<MaterialCommunityIcons name="instagram" size={35} color="#3EC6FF" />
						<Text style={styles.textProfile}>hafidz_ef</Text>
					</View>
					<View style={styles.bawah}>
						<MaterialCommunityIcons name="twitter" size={35} color="#3EC6FF" />
						<Text style={styles.textProfile}>-</Text>
					</View>
				</View>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container:{
		padding: 15,
		flex: 1
	},
	isiPorto1:{
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems:'center'
	},
	isiPorto2:{
		flexDirection: 'row',
		justifyContent: 'space-around',
		paddingRight : 40
	},
	bawah:{
		flexDirection: 'row',
		paddingLeft: 100		
	},
	textProfile:{
		fontSize:15,
	},
	textPorto:{
		fontSize:15,
		paddingLeft:3,
		paddingBottom:3,
		borderBottomColor: 'black',
        borderBottomWidth: 1,
	},
	contact:{
		marginTop:10,
		backgroundColor:'#EFEFEF',
		height: 175,
		borderRadius:10,
	},
	portofolio:{
		marginTop:10,
		backgroundColor:'#EFEFEF',
		height: 120,
		borderRadius:10,
	},
	textForm:{
		fontSize:30,
		fontWeight: "bold",
		alignItems: 'center',
		justifyContent: 'center',
		marginRight:40,
	    marginLeft:70,
	    marginTop:10,
	    paddingTop:10,
	    paddingBottom:10,
		color: '#003366'
	},
	textName:{
		fontSize:25,
		fontWeight: "bold",
		alignItems: 'center',
		justifyContent: 'center',
		marginRight:40,
	    marginLeft:60,
	    marginTop:3,
	    paddingTop:10,
	    paddingBottom:5,
		color: '#003366'
	},
	textDesc:{
		fontSize:15,
		fontWeight: "bold",
		alignItems: 'center',
		justifyContent: 'center',
		marginRight:40,
	    marginLeft:90,
	    
		color: '#3EC6FF'
	},
	profileIcon:{
		marginRight:40,
	    marginLeft:110,
	}
});