import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';

import data from './data.json';
import VideoItem from './components/skillitem';
// import data from './skillData.json';

const DEVICE = Dimensions.get('window')

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      searchText: '',
      totalPrice: 0,
    }
  }

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  updatePrice(price) {
    //? #Soal Bonus (10 poin) 
    //? Buatlah teks 'Total Harga' yang akan bertambah setiap kali salah satu barang/item di klik/tekan.
    //? Di sini, buat fungsi untuk menambahkan nilai dari state.totalPrice dan ditampilkan pada 'Total Harga'.
    
    // Kode di sini
  }


  render() {
    console.log(data)
    return (
      <View style={styles.container}>
        <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 20, marginVertical: 8 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text>Hai,{'\n'}
            {/* //? #Soal 1 Tambahan, Simpan userName yang dikirim dari halaman Login pada komponen Text di bawah ini */}
              <Text style={styles.headerText}>tempat userName</Text>
            </Text>

            {/* //? #Soal Bonus, simpan Total Harga dan state.totalPrice di komponen Text di bawah ini */}
            <Text style={{ textAlign: 'right' }}>Total Harga{'\n'}
              <Text style={styles.headerText}>tempat total harga</Text>
            </Text>
          </View>
          <View>

          </View>
          <TextInput
            style={{ backgroundColor: 'white', marginTop: 8 }}
            placeholder='Cari barang..'
            onChangeText={(searchText => this.setState({ searchText }))}
          />
        </View>
          <FlatList
            data={data.produk}
            renderItem={(data)=>
              <ListItem data={data.item} />
            }
            keyExtractor={(item)=>item.id}
            itemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
          />

      </View>
    )
  }
};

class ListItem extends React.Component {

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  //? #Soal No 3 (15 poin)
  //? Buatlah styling komponen ListItem, agar dapat tampil dengan baik di device

  render() {
    const data = this.props.data
    return (
      <View style={styles.itemContainer}>
        <View style={styles.pisah}>
          <View>
            <Image source={{ uri: data.gambaruri }} style={styles.itemImage} resizeMode='contain' />
          </View>
          <View style={styles.marge}>
            <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{data.nama}</Text>
            <Text style={styles.itemPrice}>{this.currencyFormat(Number(data.harga))}</Text>
            <Text style={styles.itemStock}>Sisa stok: {data.stock}</Text>
          </View>
        </View>
        <Button style={styles.beli} title='BELI' color='blue' />
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  marge:{
    margin:10,
    width:220
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold'
  },

  //? Lanjutkan styling di sini
  itemContainer: {
    width: DEVICE.width * 0.44,
    backgroundColor: '#B4E9FF',
    marginLeft:2,
    marginRight:2,
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff',
    width : 325,
    
  },
  pisah:{
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems:'center'
  },
  itemImage: {
    width:100, 
    height: 100,
    backgroundColor: '#B4E9FF',
  },
  itemName: {
    fontSize:20, 
    color:'#003366'
  },
  itemPrice: {
    fontSize:16, 
    color:'#003366'
  },
  itemStock: {
    fontSize:16, 
    color:'#003366'
  },
  itemButton: {
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff',
    backgroundColor : '#003366'
  },
  buttonText: {
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff',
    backgroundColor : '#003366'
  },
})
