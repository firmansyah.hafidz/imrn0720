//SOALm 1/////////////////////////////////////////////
class Animal {
    constructor(name) {
        this.animalName = name
        this.animalLegs = 4
        this.animalColdblooded = false
    }
    get name() {
    	return this.animalName;
  	}
  	get legs() {
    	return this.animalLegs;
  	}
  	get cold_blooded(){
  		return this.animalColdblooded;
  	}
}

class Ape extends Animal {
	
 	constructor(name) {
        super(name)
        this.animalLegs = 2
    }

	yell(){
		return console.log("Auooo");
	}

}

class Frog extends Animal {

	jump(){
		return console.log("hop hop");
	}

}
 
var sheep = new Animal("shaun");
 
console.log("----RELEASE 0----")
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log("----RELEASE 1----")
var sungokong = new Ape("kera sakti")
console.log("Jumlah kaki "+sungokong.name+" : "+sungokong.legs)
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

//SOAL 2////////////////////////////////////////////

class Clock {
	constructor({ template }){
		this._template = template.toString();
	}

	render(){
		var date = new Date();

	    var hours = date.getHours();
	    if (hours < 10) hours = '0' + hours;

	    var mins = date.getMinutes();
	    if (mins < 10) mins = '0' + mins;

	    var secs = date.getSeconds();
	    if (secs < 10) secs = '0' + secs;

	    var output = this._template
	      .replace('h', hours)
	      .replace('m', mins)
	      .replace('s', secs);

	    console.log(output);
	}

	stop() {
    	clearInterval(timer);
  	};

  	start() {
    	this.render();
    	var timer = setInterval(this.render.bind(this), 1000);
  	};

}

var clock = new Clock({template: 'h:m:s'});
clock.start();  

