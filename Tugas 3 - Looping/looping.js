//SOAL 1////////////////////////////////////////////////////////////////////

var i = 1;
console.log("LOOPING PERTAMA");
while (i<=10){
	temp = i*2;
	console.log(temp+" - I love coding");
	i++;
}
var i = 10;
console.log("LOOPING KEDUA");
while (i>0){
	temp = i*2;
	console.log(temp+" - I will become a mobile developer");
	i--;
}

//SOAL 2//////////////////////////////////////////////////////////////////

for(i=0;i<=20;i++){
	if(i%2!=0){
		if((i%3==0)&&(i%2!=0)){
			console.log(i+" - I Love Coding");
		} else {
			console.log(i+" - Santai");
		}
	} else if(i%2==0){
		console.log(i+" - Berkualitas");
	}		
}

//SOAL 3/////////////////////////////////////////////////////////////////

var temp = "";
for(i=0;i<4;i++){
	for(j=0;j<8;j++){
		temp +="#"; 
	}
	console.log(temp);
	temp = "";
}

//SOAL 4////////////////////////////////////////////////////////////////

var temp = "";
for(i=0;i<7;i++){
	temp +="#";
	console.log(temp);
}

//SOAL 5////////////////////////////////////////////////////////////////

var temp = "";
for(i=0;i<8;i++){
	if(i%2!=0){
		for(j=0;j<4;j++){
			temp +=" #";
		}
	} else {
		for(j=0;j<4;j++){
			temp +="# ";
		}
	}	
	console.log(temp);
	temp = "";
}








































	