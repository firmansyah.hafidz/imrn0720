
//SOAL 1//////////////////////////////////////////////////////////////////////////////////
var nama = "Jenita"
var peran = "Guard"

// Output untuk Input nama = '' dan peran = ''
if(nama == ""){
	console.log("Nama harus diisi!");
} else {
	console.log("Nama kamu "+nama);
}

if(peran == ""){
	console.log("Peran harus diisi!");
} else {
	console.log("Peran kamu "+peran);
}

if((nama!="")&&(peran!="")){
	if((nama == "Jane") &&(peran == "Penyihir")){
		console.log("Selamat datang di Dunia Werewolf, Jane");
		console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!");
	} else if((nama == "Jenita") &&(peran == "Guard")){
		console.log("Selamat datang di Dunia Werewolf, Jenita");
		console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.");
	} else if((nama == "Junaedi") &&(peran == "Werewolf")){
		console.log("Selamat datang di Dunia Werewolf, Junaedi");
		console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!" );
	} else if ((nama!="") && (peran!="")){
		console.log("Selamat datang di Dunia Werewolf, "+nama);
		console.log("Halo "+nama+", peran kamu adalah "+peran);
	} else if ((nama!="") && (peran=="")){
		console.log("Selamat datang di Dunia Werewolf, "+nama);
		console.log("Peran harus diisi!");
	} else if (nama=="") {
		console.log("Nama harus diisi!");
	}
}

//SOAL 2 ////////////////////////////////////////////////////////////////////////////////////////
var tanggal = 3;
var bulan = 12;
var tahun = 2020;

if((tanggal<0)||(tanggal>31)||(bulan<0)||(bulan>12)||(tahun<1000)){
	console.log("tanggal tidak valid");
} else {
	switch(bulan) {
	 case 1: bulan = "Januari"; break;
	 case 2: bulan = "Februari"; break;
	 case 3: bulan = "Maret"; break;
	 case 4: bulan = "April"; break;
	 case 5: bulan = "Mei"; break;
	 case 6: bulan = "Juni"; break;
	 case 7: bulan = "Juli"; break;
	 case 8: bulan = "Agustus"; break;
	 case 9: bulan = "September"; break;
	 case 10: bulan = "Oktober"; break;
	 case 11: bulan = "November"; break;
	 case 12: bulan = "Desember"; break;
	}

	console.log(tanggal+" "+bulan+" "+tahun);
}





































