// SOAL 1//////////////////////////////////////////

function balikString(str) {
var newString = "";
	for (var i = str.length - 1; i >= 0; i--) { 
		newString += str[i];
	}
	return newString;
}

console.log(balikString("Javascript"));
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah



//SOAL 2//////////////////////////////////////////

function palindrome(str) {
 var re = /[^A-Za-z0-9]/g;
 str = str.toLowerCase().replace(re, '');
 var len = str.length;
 for (var i = 0; i < len/2; i++) {
   if (str[i] !== str[len - 1 - i]) {
       return false;
   }
 }
 return true;
}
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false

//SOAL 3//////////////////////////////////////////

function bandingkan(bil1 = 0, bil2 = 0){
	var hasil = 0;
	if((typeof bil1 === 'string')||(typeof bil2 === 'string')){
		if(typeof bil1 === 'string'){
			var param1 = parseInt(bil1)
		}
		if(typeof bil2 === 'string'){
			var param2 = parseInt(bil2)
		}
		if(param1>param2){
			return hasil = param1
		} else if(param2>param1){
			return hasil = param2
		}

	} else{
		if((bil1<0)||(bil2<0)){
			return hasil = -1;
		} else if((bil1==0)&&(bil2==0)){
			return hasil = -1
		} else if(bil1==bil2){
			return hasil = -1
		} else if(bil1>bil2){
			return hasil = bil1
		} else if(bil2>bil1){
			return hasil = bil2
		} else {
			return hasil = 1
		}
	}
}

console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18


//SOAL 4/////////////////////////////////////////////////////////

function DescendingTen(angka = 0){
	var wadah = '';
	if(angka == 0){
		wadah += -1
	} else {
		for(i=0;i<10;i++){
			wadah += angka--
			wadah += " "
		}
	}
	if(typeof wadah === 'string'){
		return wadah
	}
}

console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1





































